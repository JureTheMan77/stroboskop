# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://JureTheMan77@bitbucket.org/JureTheMan77/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/JureTheMan77/stroboskop/commits/fc8b9fa79834f1a4b265ef9f53073cdb2896551c

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/JureTheMan77/stroboskop/commits/daece1c76304179a37e13d4785863f699a67fe13

Naloga 6.3.2:
https://bitbucket.org/JureTheMan77/stroboskop/commits/57c30c1e31af4c39240fdfee23b93ae51bbd9c02

Naloga 6.3.3:
https://bitbucket.org/JureTheMan77/stroboskop/commits/76efea82cdac1d147b938bc932166ec3e953cabb

Naloga 6.3.4:
https://bitbucket.org/JureTheMan77/stroboskop/commits/6cc4c94b054261028affa6795e588182895f349f

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/JureTheMan77/stroboskop/commits/3c1d93f12601516284124bbaf497d227b2b7cc26

Naloga 6.4.2:
https://bitbucket.org/JureTheMan77/stroboskop/commits/3c1d93f12601516284124bbaf497d227b2b7cc26

Naloga 6.4.3:
https://bitbucket.org/JureTheMan77/stroboskop/commits/aafadd4cb75eac56658513a906432e6105517724

Naloga 6.4.4:
https://bitbucket.org/JureTheMan77/stroboskop/commits/ae72b0a18d9674aa0ff43718a50033f04e21dac9